package filter.visitors;

import com.tangosol.util.Filter;
import com.tangosol.util.ValueExtractor;
import com.tangosol.util.extractor.PofExtractor;
import com.tangosol.util.filter.AllFilter;
import com.tangosol.util.filter.AndFilter;
import com.tangosol.util.filter.AnyFilter;
import com.tangosol.util.filter.BetweenFilter;
import com.tangosol.util.filter.EqualsFilter;
import com.tangosol.util.filter.GreaterEqualsFilter;
import com.tangosol.util.filter.GreaterFilter;
import com.tangosol.util.filter.InFilter;
import com.tangosol.util.filter.LessEqualsFilter;
import com.tangosol.util.filter.LessFilter;
import com.tangosol.util.filter.LikeFilter;
import com.tangosol.util.filter.NotEqualsFilter;
import com.tangosol.util.filter.NotFilter;
import com.tangosol.util.filter.OrFilter;
import filter.resolvers.ColumnResolver;
import filter.util.Converter;
import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.Union;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Abstract base class for all Coherence style ValueExtractors, given a
 * SQL like statement it will produce a Filter in {@link #getFilter()}.
 * <p/>
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 */
public class AbstractFilterVisitor
        implements FilterVisitor {

    /**
     * Column resolver used to create the ValueExtractor.
     */
    private final ColumnResolver resolver;
    /**
     * Converter used to ensureExpression values (right side) that cannot be otherwise identified.
     */
    private final Converter converter;
    /**
     * The expression stack.
     */
    private Stack<Object> expressionStack = new Stack<Object>();

    /**
     * Create a new PofFilterVisitor using specifying the <tt>valueType</tt>
     * and optionally specifying the <tt>keyType</tt>
     *
     * @param resolver  resolver to use to create the correct ValueExtractor
     * @param converter an optional converted used to convert unidentified right hand expressions
     */
    protected AbstractFilterVisitor(ColumnResolver resolver, Converter converter) {
        this.resolver = resolver;
        this.converter = converter;
    }

    /**
     * Retrieve the evaluated filter.
     *
     * @return the filter rendered from the string representation
     */
    @Override
    public Filter getFilter() {
        return (Filter) expressionStack.peek();
    }

    @Override
    public void visit(Select select) {
        select.getSelectBody().accept(this);
    }

    @Override
    public void visit(NullValue nullValue) {
        expressionStack.push(nullValue);
    }

    @Override
    public void visit(JdbcParameter jdbcParameter) {
        expressionStack.push(jdbcParameter.toString());
    }

    @Override
    public void visit(DoubleValue doubleValue) {
        expressionStack.push(doubleValue.getValue());
    }

    @Override
    public void visit(LongValue longValue) {
        expressionStack.push(longValue.getValue());
    }

    @Override
    public void visit(DateValue dateValue) {
        expressionStack.push(dateValue.getValue());
    }

    @Override
    public void visit(TimeValue timeValue) {
        expressionStack.push(timeValue.getValue());
    }

    @Override
    public void visit(TimestampValue timestampValue) {
        expressionStack.push(timestampValue.getValue());
    }

    @Override
    public void visit(StringValue stringValue) {
        expressionStack.push(stringValue.getValue());
    }

    @Override
    public void visit(Parenthesis parenthesis) {
        Stack oldExpressionStack = expressionStack;
        expressionStack = new Stack<Object>();
        parenthesis.getExpression().accept(this);
        oldExpressionStack.push(expressionStack.pop());
        expressionStack = oldExpressionStack;
    }

    @Override
    public void visit(AndExpression andExpression) {
        ensureExpression(andExpression.getLeftExpression(), andExpression.getRightExpression());
        expressionStack.push(negate(andExpression,
                                    new AndFilter((Filter) expressionStack.pop(), (Filter) expressionStack.pop())));
    }

    @Override
    public void visit(OrExpression orExpression) {
        ensureExpression(orExpression.getLeftExpression(), orExpression.getRightExpression());
        expressionStack.push(negate(orExpression,
                                    new OrFilter((Filter) expressionStack.pop(), (Filter) expressionStack.pop())));
    }

    @Override
    public void visit(EqualsTo equalsTo) {
        ensureExpression(equalsTo.getLeftExpression(), equalsTo.getRightExpression());
        expressionStack.push(
                negate(equalsTo, new EqualsFilter((ValueExtractor) expressionStack.pop(), expressionStack.pop())));
    }

    @Override
    public void visit(GreaterThan greaterThan) {
        ensureExpression(greaterThan.getLeftExpression(), greaterThan.getRightExpression());
        expressionStack.push(negate(greaterThan,
                                    new GreaterFilter((ValueExtractor) expressionStack.pop(),
                                                      (Comparable) expressionStack.pop())));
    }

    @Override
    public void visit(GreaterThanEquals greaterThanEquals) {
        ensureExpression(greaterThanEquals.getLeftExpression(), greaterThanEquals.getRightExpression());
        expressionStack.push(negate(greaterThanEquals,
                                    new GreaterEqualsFilter((ValueExtractor) expressionStack.pop(),
                                                            (Comparable) expressionStack.pop())));
    }

    @Override
    public void visit(MinorThan minorThan) {
        ensureExpression(minorThan.getLeftExpression(), minorThan.getRightExpression());
        expressionStack.push(negate(minorThan,
                                    new LessFilter((ValueExtractor) expressionStack.pop(),
                                                   (Comparable) expressionStack.pop())));
    }

    @Override
    public void visit(MinorThanEquals minorThanEquals) {
        ensureExpression(minorThanEquals.getLeftExpression(), minorThanEquals.getRightExpression());
        expressionStack.push(negate(minorThanEquals,
                                    new LessEqualsFilter((ValueExtractor) expressionStack.pop(),
                                                         (Comparable) expressionStack.pop())));
    }

    @Override
    public void visit(NotEqualsTo notEqualsTo) {
        ensureExpression(notEqualsTo.getLeftExpression(), notEqualsTo.getRightExpression());
        expressionStack.push(new NotEqualsFilter((ValueExtractor) expressionStack.pop(), expressionStack.pop()));

    }

    @Override
    public void visit(Between between) {
        if (between.getBetweenExpressionStart() instanceof Column
            && between.getBetweenExpressionEnd() instanceof Column) {
            between.getLeftExpression().accept(this);
            PofExtractor extractor = (PofExtractor) expressionStack.pop();

            expressionStack.push(converter.convert(extractor.getClassExtracted(),
                                                   ((Column) between.getBetweenExpressionEnd()).getColumnName()));
            expressionStack.push(converter.convert(extractor.getClassExtracted(),
                                                   ((Column) between.getBetweenExpressionStart()).getColumnName()));
            expressionStack.push(extractor);
        } else {
            between.getBetweenExpressionEnd().accept(this);
            between.getBetweenExpressionStart().accept(this);
            between.getLeftExpression().accept(this);
        }

        expressionStack.push(negate(between.isNot(),
                                    new BetweenFilter((ValueExtractor) expressionStack.pop(),
                                                      (Comparable) expressionStack.pop(),
                                                      (Comparable) expressionStack.pop())));
    }

    @Override
    public void visit(InExpression inExpression) {
        inExpression.getItemsList().accept(this);
        inExpression.getLeftExpression().accept(this);

        expressionStack.push(negate(inExpression.isNot(),
                                    new InFilter((ValueExtractor) expressionStack.pop(), ensureValues(
                                            (Collection<Object>) expressionStack.pop()))));
    }

    @Override
    public void visit(IsNullExpression isNullExpression) {
        isNullExpression.getLeftExpression().accept(this);

        expressionStack.push(
                negate(isNullExpression.isNot(), new EqualsFilter((ValueExtractor) expressionStack.pop(), null)));

    }

    @Override
    public void visit(LikeExpression likeExpression) {
        likeExpression.getRightExpression().accept(this);
        likeExpression.getLeftExpression().accept(this);

        expressionStack.push(negate(likeExpression,
                                    new LikeFilter((ValueExtractor) expressionStack.pop(),
                                                   (String) expressionStack.pop(), '\\', true)));

    }

    @Override
    public void visit(Column tableColumn) {
        expressionStack.push(resolver.resolve(tableColumn));

    }

    @Override
    public void visit(AllComparisonExpression allComparisonExpression) {
        allComparisonExpression.accept(this);
        expressionStack.push(new AllFilter((Filter[]) expressionStack.pop()));
    }

    @Override
    public void visit(AnyComparisonExpression anyComparisonExpression) {
        anyComparisonExpression.accept(this);
        expressionStack.push(new AnyFilter((Filter[]) expressionStack.pop()));
    }

    @Override
    public void visit(PlainSelect plainSelect) {
        plainSelect.getFromItem().accept(this);
        plainSelect.getWhere().accept(this);
    }

    @Override
    public void visit(ExpressionList expressionList) {
        expressionStack.push(new HashSet<Object>(expressionList.getExpressions()));
    }

    @Override
    public void visit(Concat concat) {
    }

    @Override
    public void visit(Matches matches) {
    }

    @Override
    public void visit(BitwiseAnd bitwiseAnd) {
    }

    @Override
    public void visit(BitwiseOr bitwiseOr) {
    }

    @Override
    public void visit(BitwiseXor bitwiseXor) {
    }

    @Override
    public void visit(SubJoin subjoin) {
    }

    @Override
    public void visit(Union union) {
    }

    @Override
    public void visit(CaseExpression caseExpression) {
    }

    @Override
    public void visit(WhenClause whenClause) {
    }

    @Override
    public void visit(ExistsExpression existsExpression) {
    }

    @Override
    public void visit(SubSelect subSelect) {
    }

    @Override
    public void visit(Addition addition) {
    }

    @Override
    public void visit(Division division) {
    }

    @Override
    public void visit(Multiplication multiplication) {
    }

    @Override
    public void visit(Subtraction subtraction) {
    }

    @Override
    public void visit(Function function) {
    }

    @Override
    public void visit(InverseExpression inverseExpression) {
    }

    @Override
    public void visit(Delete delete) {
    }

    @Override
    public void visit(Update update) {
    }

    @Override
    public void visit(Insert insert) {
    }

    @Override
    public void visit(Replace replace) {
    }

    @Override
    public void visit(Drop drop) {
    }

    @Override
    public void visit(Truncate truncate) {
    }

    @Override
    public void visit(CreateTable createTable) {
    }

    @Override
    public void visit(Table tableName) {
    }

    /* ------ helpers ------ */

    private Filter negate(BinaryExpression expression, Filter filter) {
        return negate(expression.isNot(), filter);
    }

    private Filter negate(boolean not, Filter filter) {
        return not ? new NotFilter(filter) : filter;
    }

    private void ensureExpression(Expression lSide, Expression rSide) {
        if (rSide instanceof Column || rSide instanceof StringValue) {
            // since right side expression do not exists
            // it needs to be converted into a value using the specified converter
            lSide.accept(this);
            PofExtractor extractor = (PofExtractor) expressionStack.pop();

            Class<?> type = extractor.getClassExtracted();

            String stringRep = rSide instanceof Column
                               ? ((Column) rSide).getColumnName()
                               : ((StringValue) rSide).getValue();

            Object convertItem = converter.convert(type, stringRep);
            expressionStack.push(convertItem == null ? stringRep : convertItem);

            expressionStack.push(extractor);
        } else {
            // first process right side
            rSide.accept(this);
            // right side should be on top of the stack still
            // just accept the left side as is
            lSide.accept(this);
        }
    }

    private Set ensureValues(Collection<Object> values) {
        Set<Object> convertedValues = new HashSet<Object>(values.size());
        PofExtractor extractor = (PofExtractor) expressionStack.peek();

        for (Object value : values) {
            if (value instanceof Column) {
                convertedValues.add(converter.convert(extractor.getClassExtracted(), value.toString()));
            } else {
                convertedValues.add(value);
            }
        }
        return convertedValues;
    }
}
