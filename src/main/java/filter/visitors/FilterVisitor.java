package filter.visitors;

import com.tangosol.util.Filter;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitor;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 5:34 PM
 */
public interface FilterVisitor
        extends StatementVisitor, FromItemVisitor, SelectVisitor, ExpressionVisitor, ItemsListVisitor {
    Filter getFilter();
}
