package filter.visitors;

import filter.resolvers.EntryColumnResolver;
import filter.resolvers.KeyResolver;
import filter.resolvers.ValueResolver;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 9:26 PM
 */
public class EntryFilterVisitor
        extends AbstractFilterVisitor {
    /**
     * Create a new EntryFilterVisitor.
     */
    public EntryFilterVisitor() {
        super(new EntryColumnResolver(new KeyResolver(), new ValueResolver()), null);
    }
}
