package filter.visitors.coherence;

import filter.resolvers.EntryColumnResolver;
import filter.resolvers.coherence.PofKeyResolver;
import filter.resolvers.coherence.PofValueResolver;
import filter.util.Converter;
import filter.visitors.AbstractFilterVisitor;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 9:34 PM
 */
public class PofFilterVisitor
    extends AbstractFilterVisitor {

    /**
     * Create a new PofFilterVisitor using specifying the <tt>valueType</tt>
     * and optionally specifying the <tt>keyType</tt>
     *
     * @param keyType   an optional key valueType
     * @param valueType the value valueType
     * @param converter an optional Converter used to convert values that cannot be identified
     */
    public PofFilterVisitor(Class<?> keyType, Class<?> valueType, Converter converter) {
        super(new EntryColumnResolver(new PofKeyResolver(keyType), new PofValueResolver(valueType)), converter);
    }
}
