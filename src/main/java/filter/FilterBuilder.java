package filter;

import com.tangosol.util.Filter;
import filter.visitors.FilterVisitor;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;

import java.io.StringReader;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 5:27 PM
 */
public abstract class FilterBuilder {

    public static Filter build(String statement, FilterVisitor visitor)
            throws JSQLParserException {
        CCJSqlParserManager pm = new CCJSqlParserManager();

        pm.parse(new StringReader(sanitize(statement))).accept(visitor);
        return visitor.getFilter();
    }

    private static String sanitize(String statement) {
        if (statement.toLowerCase().startsWith("select ")) {
            return statement;
        } else if (statement.toLowerCase().startsWith("from ")) {
            return "select * " + statement;
        } else if (statement.toLowerCase().startsWith("where ")) {
            return "select * from x " + statement;
        }
        return "select * from x where " + statement;
    }
}
