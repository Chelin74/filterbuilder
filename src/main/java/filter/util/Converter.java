package filter.util;

/**
 * User: charlie.helin@gmail.com
 */
public interface Converter {
    /**
     * Transform the passed in <tt>string</tt> into an instance
     * of the specified <tt>type</tt>.
     *
     * @param type   the type to transform into
     * @param string the string that represent the instance
     * @return an instance of the <tt>type</tt> or <tt>null</tt> if it cannot
     *         be transformed
     */
    <T> T convert(Class<T> type, String string);
}
