package filter.resolvers.coherence;

import com.tangosol.io.pof.annotation.PortableProperty;
import com.tangosol.util.ValueExtractor;
import filter.resolvers.ColumnResolver;
import net.sf.jsqlparser.schema.Column;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: charlie.helin@gmail.com
 * Date: 7/20/13
 * Time: 8:19 AM
 */
public abstract class AbstractPofResolver
        implements ColumnResolver {

    protected static Map<String, ValueExtractor> extractorCache = new ConcurrentHashMap<String, ValueExtractor>();
    protected final Class<?> type;

    protected AbstractPofResolver(Class<?> type) {
        this.type = type;
    }

    /**
     * Retrieve the key type.
     *
     * @return the key type
     */
    public Class<?> getType() {
        return type;
    }

    @Override
    public ValueExtractor resolve(Column column) {
        String typeName = getType().getSimpleName();
        String wholeColumnName = column.getWholeColumnName();
        String cacheKey = typeName + ":" + wholeColumnName;

        ValueExtractor valueExtractor = extractorCache.get(cacheKey);
        if (valueExtractor == null) {
            String[] parts = wholeColumnName.split("\\.");

            if (type == null) {
                throw new IllegalArgumentException("Type have not been specified/determined.");
            }

            // skip key
            int length = parts.length;
            List<Integer> list = new ArrayList<Integer>(length);

            IndexTuple indexTuple = null;
            for (String part : parts) {
                boolean keyOrSelf = isKeyOrSelf(part);
                if (length == 1 && keyOrSelf) {
                    return createInstanceExtractor();
                } else if (!keyOrSelf) {
                    Class<?> type = indexTuple == null ? getType() : indexTuple.type;
                    indexTuple = getIndex(part, type);
                    int index = indexTuple.index;

                    if (index == -1) {
                        throw new IllegalArgumentException(
                                "Unknown path '"
                                + part + "' in " + wholeColumnName
                                + " (" + indexTuple + ")");
                    }

                    list.add(index);
                }
            }

            int[] path = toPath(list);
            valueExtractor = createPofExtractor(indexTuple.type, path);
            extractorCache.put(cacheKey, valueExtractor);
        }
        return valueExtractor;
    }

    /**
     * Instantiate a ValueExtractor for the given the specified <tt>path</tt>. The ValueExtractor
     * will extract values of the <tt>type</tt>-type.
     *
     * @param type the type that will be extracted by the ValueExtractor
     * @param path the path that should be extracted
     * @return a ValueExtractor
     */
    protected abstract ValueExtractor createPofExtractor(Class<?> type, int[] path);

    /**
     * Instantiate a specific ValueExtractor that can be used to extract the instance itself.
     *
     * @return a ValueExtractor used to extract the specific extractor used to retrieve itself
     */
    protected abstract ValueExtractor createInstanceExtractor();

    /**
     * Resolve the specified <tt>column</tt> of the <tt>type</tt>, the column is supposed
     * to correspond to a field with the PortableProperty annotation.
     * If the column cannot be located in <tt>type</tt>, the base types will be examined until
     * either the <tt>column</tt> is found or <tt>type</tt> is Object. Then the returned IndexTuple
     * will have an index of -1.
     *
     * @param column the column to resolve
     * @param type   the type to resolve the column in
     * @return an IndexTuple
     */
    protected IndexTuple getIndex(String column, Class<?> type) {
        while (!type.equals(Object.class)) {
            try {
                Field field = type.getDeclaredField(column);
                type = field.getType();
                return new IndexTuple(field.getAnnotation(PortableProperty.class).value(), type);
            } catch (NoSuchFieldException e) {
                type = type.getSuperclass();
            }
        }
        return new IndexTuple(-1, type);
    }

    /**
     * Converts the specified list of Integers into an array
     * of <tt>int</tt>s.
     *
     * @param list the list to convert
     * @return an array of int
     */
    private int[] toPath(List<Integer> list) {
        int[] path = new int[list.size()];

        for (int i = 0; i < path.length; i++) {
            path[i] = list.get(i);
        }
        return path;
    }

    /**
     * Determine if the <tt>part</tt> is either a key or self.
     *
     * @param part the part to examine
     * @return <tt>true</tt> iff; the part is key or self
     */
    private boolean isKeyOrSelf(String part) {
        return part.equals(SELF) || part.equals(KEY);
    }

    protected class IndexTuple {
        public int index;
        public Class<?> type;

        public IndexTuple(int index, Class<?> type) {
            this.index = index;
            this.type = type;
        }

        @Override
        public String toString() {
            return "IndexTuple{" +
                   "index=" + index +
                   ", type=" + type +
                   '}';
        }
    }
}
