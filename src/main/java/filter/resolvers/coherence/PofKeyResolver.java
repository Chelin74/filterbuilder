package filter.resolvers.coherence;

import com.tangosol.io.pof.reflect.SimplePofPath;
import com.tangosol.util.ValueExtractor;
import com.tangosol.util.extractor.AbstractExtractor;
import com.tangosol.util.extractor.IdentityExtractor;
import com.tangosol.util.extractor.KeyExtractor;
import com.tangosol.util.extractor.PofExtractor;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 8:45 PM
 */
public class PofKeyResolver
        extends AbstractPofResolver {

    /**
     * Create a new PofKeyResolver using the specified
     * <tt>type</tt>.
     *
     * @param type of the key to
     */
    public PofKeyResolver(Class<?> type) {
        super(type);
    }

    @Override
    protected ValueExtractor createPofExtractor(Class<?> type, int[] path) {
        return new PofExtractor(type, new SimplePofPath(path), AbstractExtractor.KEY);
    }

    @Override
    protected ValueExtractor createInstanceExtractor() {
        return new KeyExtractor(IdentityExtractor.INSTANCE);
    }

    @Override
    public String toString() {
        return "PofKeyResolver{" +
               "type=" + getType() +
               '}';
    }
}
