package filter.resolvers;

import com.tangosol.util.ValueExtractor;
import com.tangosol.util.extractor.AbstractExtractor;
import com.tangosol.util.extractor.ReflectionExtractor;
import net.sf.jsqlparser.schema.Column;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 8:45 PM
 */
public class ValueResolver
        implements ColumnResolver {

    @Override
    public ValueExtractor resolve(Column column) {
        return new ReflectionExtractor(column.getWholeColumnName(), null, AbstractExtractor.VALUE);
    }

}
