package filter.resolvers;

import com.tangosol.util.ValueExtractor;
import net.sf.jsqlparser.schema.Column;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 8:40 PM
 */
public interface ColumnResolver {
    String SELF = "__self";
    String KEY = "__key";

    ValueExtractor resolve(Column column);
}
