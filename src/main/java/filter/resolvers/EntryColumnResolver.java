package filter.resolvers;

import com.tangosol.util.ValueExtractor;
import net.sf.jsqlparser.schema.Column;

/**
 * The EntryColumnResolver is specialized to be used on {@link java.util.Map.Entry entry}
 * structures. The presumption is that the key is denoted by &quot;KEY.&quot;, and
 * values are implicit.
 * <p/>
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 8:55 PM
 */
public class EntryColumnResolver
        implements ColumnResolver {

    /**
     * Resolver used to resolve the key part of an entry, can be
     * null if not needed.
     */
    private final ColumnResolver keyResolver;

    /**
     * Resolver used to resolve the value part of an entry, can be null if
     * not needed.
     */
    private final ColumnResolver valueResolver;

    /**
     * Creates a EntryColumnResolver.
     *
     * @param keyResolver   the resolver for the key part
     * @param valueResolver the resolver for the value part
     */
    public EntryColumnResolver(ColumnResolver keyResolver, ColumnResolver valueResolver) {
        this.keyResolver = keyResolver;
        this.valueResolver = valueResolver;
    }

    @Override
    public ValueExtractor resolve(Column column) {
        return column.getWholeColumnName().startsWith(KEY)
               ? keyResolver.resolve(column)
               : valueResolver.resolve(column);
    }

    @Override
    public String toString() {
        return "EntryColumnResolver{" +
               "keyResolver=" + keyResolver +
               ", valueResolver=" + valueResolver +
               '}';
    }
}
