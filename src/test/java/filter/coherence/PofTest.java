package filter.coherence;

import com.tangosol.util.Filter;
import com.tangosol.util.ValueExtractor;
import com.tangosol.util.extractor.PofExtractor;
import com.tangosol.util.filter.AndFilter;
import com.tangosol.util.filter.ArrayFilter;
import com.tangosol.util.filter.BetweenFilter;
import com.tangosol.util.filter.EqualsFilter;
import com.tangosol.util.filter.GreaterFilter;
import com.tangosol.util.filter.LessFilter;
import com.tangosol.util.filter.NotEqualsFilter;
import filter.FilterBuilder;
import filter.util.Converter;
import filter.visitors.coherence.PofFilterVisitor;
import model.Instrument;
import model.inner.MyEnum;
import net.sf.jsqlparser.JSQLParserException;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 10:36 PM
 */
public class PofTest {

    private class MyConverter implements Converter {

        @Override
        public <T> T convert(Class<T> type, String string) {
            if (type == MyEnum.class) {
                return (T) MyEnum.valueOf(string);
            }
            return null;
        }

    }


    @Test
    public void testKey()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("__key.type.code > 0 and __key.type.code < 1 ",
                                            new PofFilterVisitor(Instrument.class, null, new MyConverter()));
        Filter[] filters = ((ArrayFilter) filter).getFilters();
        assertEquals(2, filters.length);
        assertTrue(filters[0] instanceof GreaterFilter);
        GreaterFilter geFilter = (GreaterFilter) filters[0];
        ValueExtractor valueExtractor = geFilter.getValueExtractor();
        assertTrue(valueExtractor instanceof PofExtractor);
        PofExtractor extractor = (PofExtractor) valueExtractor;
        assertEquals(int.class, extractor.getClassExtracted());

        assertTrue(filters[1] instanceof LessFilter);
        LessFilter leFilter = (LessFilter) filters[1];
        valueExtractor = leFilter.getValueExtractor();
        assertTrue(valueExtractor instanceof PofExtractor);
        extractor = (PofExtractor) valueExtractor;
        assertEquals(int.class, extractor.getClassExtracted());
    }

    @Test
    public void testValue()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("type.name = 'code' and type.code = 10 ",
                                            new PofFilterVisitor(null, Instrument.class, new MyConverter()));
        Filter[] filters = ((ArrayFilter) filter).getFilters();
        assertEquals(2, filters.length);
        assertTrue(filters[0] instanceof EqualsFilter);
        EqualsFilter eqFilter = (EqualsFilter) filters[0];
        ValueExtractor valueExtractor = eqFilter.getValueExtractor();
        assertTrue(valueExtractor instanceof PofExtractor);
        PofExtractor extractor = (PofExtractor) valueExtractor;
        assertEquals(String.class, extractor.getClassExtracted());

        assertTrue(filters[1] instanceof EqualsFilter);
        eqFilter = (EqualsFilter) filters[1];
        valueExtractor = eqFilter.getValueExtractor();
        assertTrue(valueExtractor instanceof PofExtractor);
        extractor = (PofExtractor) valueExtractor;
        assertEquals(int.class, extractor.getClassExtracted());
    }

    @Test
    public void testMixed()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("__key.type.name = 'code' and type.code = 10 or __key.type.name = 'other'",
                                            new PofFilterVisitor(Instrument.class, Instrument.class, new MyConverter()));
        Filter[] filters = ((ArrayFilter) filter).getFilters();
        assertEquals(2, filters.length);
        assertEquals(AndFilter.class, filters[0].getClass());

        AndFilter andFilter = (AndFilter) filters[0];

        assertEquals(EqualsFilter.class, andFilter.getFilters()[0].getClass());
        assertEquals(EqualsFilter.class, andFilter.getFilters()[1].getClass());

        assertEquals(EqualsFilter.class, filters[1].getClass());
    }

    @Test
    public void testNot()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("__key.type.name != 'code'",
                                            new PofFilterVisitor(Instrument.class, String.class, new MyConverter()));

        assertTrue(filter instanceof NotEqualsFilter);
    }

    @Test
    public void testParentheses()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("( __key.type.name != 'code' or __key.type.name = 'other') and (type.name = 'code' and __key.type.name = 'code')" ,
                                            new PofFilterVisitor(Instrument.class, Instrument.class, new MyConverter()));
        System.out.println(filter);
        //assertTrue(filter instanceof NotEqualsFilter);
    }

    @Test
    public void testEnum()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build(" myEnum != VALUE",
                                            new PofFilterVisitor(Instrument.class, Instrument.class, new MyConverter()));

        assertTrue(filter instanceof NotEqualsFilter);
    }

    @Test
    public void testEnumBetween()
            throws JSQLParserException {
        Filter filter = FilterBuilder.build("myEnum BETWEEN MOO AND ZOO",
                                            new PofFilterVisitor(Instrument.class, Instrument.class, new MyConverter()));

        assertTrue(filter instanceof BetweenFilter);
    }


}
