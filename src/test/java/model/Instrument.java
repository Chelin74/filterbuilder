package model;

import com.tangosol.io.pof.annotation.PortableProperty;
import model.inner.MyEnum;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/16/13
 * Time: 10:31 PM
 */
public class Instrument {
    @PortableProperty(0)
    Foo type;

    @PortableProperty(1)
    MyEnum myEnum;
}
