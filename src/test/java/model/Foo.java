package model;

import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/13/13
 * Time: 9:18 AM
 */
public class Foo {
    @PortableProperty(1)
    int code;

    @PortableProperty(2)
    String name;
}
