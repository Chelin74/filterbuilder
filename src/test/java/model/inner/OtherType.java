package model.inner;

import com.tangosol.io.pof.annotation.PortableProperty;

import java.util.Date;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/13/13
 * Time: 9:23 AM
 */
public class OtherType {

    @PortableProperty(1)
    private String name;

    @PortableProperty(3)
    private long id;

    @PortableProperty(5)
    private Date when;
}
