package model.inner;

import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * User: charlie.helin@gmail.com
 * Date: 5/13/13
 * Time: 9:23 AM
 */
public class MyType {
    @PortableProperty(2)
    OtherType other;

    @PortableProperty(3)
    String code;
}
