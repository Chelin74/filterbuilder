package model.inner;

/**
 * User: charlie.helin@gmail.com
 * Date: 8/3/13
 * Time: 11:10 AM
 */
public enum MyEnum {
    VALUE, MOO, NAME, KEY, SOME, FOO, ZOO
}
